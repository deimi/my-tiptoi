# My TipToi collection

## Useful scripts
### Change product-id of existing gme file
```bash
export NEW_PID 4242
export GME_FILE 
printf "\x$(printf %x $((${NEW_PID}-${NEW_PID}/256*256)))\x$(printf %x $((${NEW_PID}/256)))\x00\x00" | dd of="${GME_FILE}" conv=notrunc bs=1 seek=20 status=none
```