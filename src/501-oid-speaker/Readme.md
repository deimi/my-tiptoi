# OID code speaker
This TipToi book will speak the OID code which the pen has scanned.  
This is very useful for finding unknown OID codes in a book and probably way easier then using the debug mode of the pen where the OID codes are spoken in chinese.

# Usage
Just scan the product number for this book (e.g. I used 501) and then scan any OID code of any book.  
The pen will ...
- speak the number digit by digit and not as a complete single number.  
  oid 12345 => one two three four five
- start with the highest digit.
- always speak five digits, so depending on the code it may speak a zero for the highest digit.  
  oid 4242 => 04242

# Limitations
- The book cannot speak OID codes from 1 to 1000 as those are product codes. If you scan them the pen will immediately switch to another book instead of speaking the number.  
- I only programmed OID codes from 1001 to 15000, as this is the range for valid OID codes (according to https://tttool.readthedocs.io/de/latest/konzepte.html#wie-funktioniert-der-stift).

# Important remark
Be aware that after scanning, the pen needs a short moment before it will speak the number. I think this is due to the amount of jumps I do in the script.  
This behaviour could probably be improved by decreasing the amount of jumps on the cost of script readability.

I only tested it with a 3rd generation pen.
