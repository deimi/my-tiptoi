#!/usr/bin/env python3

with open("./main.yaml", "a") as f:
    for i in range(1001,15000):
        print(f"  {i}: $oid:={i} J(16001) P(5ms)", file=f)
