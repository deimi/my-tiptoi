```bash
../../tools/tttool/tttool --image-format png --dpi 1200 --pixel-size 3 --code-dim 7 oid-code 1001-1070
xsltproc -o herma_8mm_1001-1069.svg --stringparam oidSize 2.032mm ../svg-oid-insertion.xsl empty_template_1001-1069.svg
```

```bash
../../tools/tttool/tttool --image-format png --dpi 1200 --pixel-size 3 --code-dim 7 oid-code 1101-1170
xsltproc -o herma_8mm_1101-1169.svg --stringparam oidSize 2.032mm ../svg-oid-insertion.xsl empty_template_1101-1169.svg
```
