```bash
../../tools/tttool/tttool --image-format png --dpi 1200 --pixel-size 3 --code-dim 7 oid-code 501-528
xsltproc -o herma_13mm_501-528.svg --stringparam oidSize 2.032mm ../svg-oid-insertion.xsl empty_template_501-528.svg
```
