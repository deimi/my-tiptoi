#!/bin/bash
# Creates a pdf with the needed oid codes and the gme file
# You need to manually build tttool before being able to use this script

TTTOOL=$(realpath tools/tttool/tttool)

pushd $1

# generate oid file
${TTTOOL} --code-dim 10 --image-format PDF --dpi 1200 --pixel-size 3 oid-table main.yaml oid-result.pdf

# assemble gme file
${TTTOOL} assemble main.yaml result.gme

popd
